# syntax=docker/dockerfile:experimental
#
# The arguments in this file are configured in
# /scripts/build-image.sh
#

ARG BUILD_IMAGE_NAME
ARG BUILD_IMAGE_VERSION
FROM ${BUILD_IMAGE_NAME}:${BUILD_IMAGE_VERSION}

ENV LANG=en_US.UTF-8 \
    LANGUAGE=en_US:en \
    LC_ALL=en_US.UTF-8

# Install / rebuild locales to make sure we have the correct language
# set within the container for C string operations.
COPY scripts/rebuild-locales.sh /usr/local/bin/
RUN /usr/local/bin/rebuild-locales.sh

COPY php/*.sh /usr/local/bin/build-php/
COPY php/php* /usr/local/etc/php/

RUN /usr/local/bin/build-php/install-base.sh
