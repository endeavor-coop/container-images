#!/usr/bin/env bash

export DOCKER_BUILDKIT=1
export CI_REGISTRY="registry.gitlab.com"
export CI_REGISTRY_IMAGE="registry.gitlab.com/endeavor-coop/container-images"
export FORCE_BUILD="${FORCE_BUILD:-true}"

if [[ -z "$CI_JOB_NAME" ]]
then
    echo -n "What is the CI_JOB_NAME [default: python-3.9] ? "
    read -r job_name
    export CI_JOB_NAME="${job_name:-python-3.9}"
fi

./scripts/build-image.sh "$CI_JOB_NAME" "--tag $CI_REGISTRY_IMAGE:$CI_JOB_NAME"
