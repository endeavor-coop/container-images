#!/usr/bin/env bash
#
# Build a container image
#
# Supported environment varaibles:
#   BUILD_CONTEXT   (str, optional): Override the image build context. Defaults to ".".
#   FORCE_BUILD     (str, optional): Set to "true" to not use container registry cache when building image. Defaults to "false".

set -xeuo pipefail
IFS=$'\n\t'

function print_arg_argument() {
    printf -- "--build-arg %s=%s " "$1" "$2"
}

function print_version_argument() {
    printf -- "--build-arg %s_VERSION=%s " "${1^^}" "$2"
}

function add_python_pip_requirement() {
    # Make sure that the version is in a valid X.Y.Z semvar format.
    VERSION=$(echo "$2.0" | sed -r -e "s/([0-9]+)\.([0-9]+).*/\1\.\2\.0/g")

    # Make sure file exists before we blindly add stuff to it.
    touch python/requirements.txt

    echo "$1~=$VERSION" >> python/requirements.txt
}

function parse_arguments() {
    read -r base
    read -r base_version
    build_context="${BUILD_CONTEXT:-.}"
    dockerfile="Dockerfile.custom"

    if [[ -f "Dockerfile.$base" ]]
    then
        dockerfile="Dockerfile.$base"
    fi

    case "$base" in
        cuda )
            case "$base_version" in
                12.4 )
                    base_version="12.4.1-base-ubuntu22.04"
                    ;;
                * )
                    base_version="12.6.1-base-ubuntu24.04"
                    ;;
            esac

            while read -r package
            do
                read -r package_version

                if [[ "$package" = "python" ]]
                then
                    case "$package_version" in
                        3.10 )
                            package_version=3.10.15
                            print_version_argument "PYTHON_GPG_KEY" "A035C8C19219BA821ECEA86B64E628F8D684696D"
                            ;;

                        3.11 )
                            package_version=3.11.10
                            print_version_argument "PYTHON_GPG_KEY" "A035C8C19219BA821ECEA86B64E628F8D684696D"
                            ;;

                        3.12 )
                            package_version=3.12.6
                            print_version_argument "PYTHON_GPG_KEY" "7169605F62C751356D054A26A821E680E5FA6305"
                            ;;
                    esac

                    # package_version=$(echo "$package_version.0" | sed -r -e "s/([0-9]+)\.([0-9]+).*/\1\.\2\.0/g")
                fi

                print_version_argument "$package" "$package_version"
            done
        ;;

        limesurvey )
            case "$base_version" in
                3.25 | 3.25.18 )
                    LIMESURVEY_VERSION='3.25.18+210316'
                    LIMESURVEY_SHA256SUM='16c5b775ffe7cab778eb10ffe8723f45762e031ced4d254508a53f691dd9bd2d'
                    ;;

                4.4 | 4.4.13 )
                    LIMESURVEY_VERSION='4.4.13+210315'
                    LIMESURVEY_SHA256SUM='f6a6af245aa4a318c7fca66a1db76add3580cd0cb56e0c4a5b1516600f214152'
                    ;;

                * )
                    LIMESURVEY_VERSION='3.25.18+210316'
                    LIMESURVEY_SHA256SUM='16c5b775ffe7cab778eb10ffe8723f45762e031ced4d254508a53f691dd9bd2d'
                    ;;
            esac

            print_arg_argument "LIMESURVEY_VERSION" "$LIMESURVEY_VERSION"
            print_arg_argument "LIMESURVEY_SHA256SUM" "$LIMESURVEY_SHA256SUM"

            ;;

        node )
            os_version="bookworm"

            if (( ${base_version%.*} < 17 ))
            then
                os_version="buster"
            fi

            base_version="$base_version-$os_version-slim"

            while read -r package
            do
                read -r package_version

                if [[ -f "node/install-$package.sh" ]]
                then
                    print_version_argument "$package" "$package_version"
                fi
            done

            ;;

        python )
            # Make sure to clean any previous requirements.txt before starting.
            rm -f python/requirements.txt

            base_version="$base_version-slim-bookworm"

            while read -r package
            do
                read -r package_version

                if [[ -f "python/requirements.$package.$package_version.txt" ]]
                then
                    print_version_argument "$package" "$package_version"
                else
                    add_python_pip_requirement "$package" "$package_version"
                fi
            done

            ;;

        php )
            base_version="$base_version-fpm"
            ;;

        * )
            ;;
    esac

    printf -- "--build-arg BUILD_IMAGE_NAME=%s " "$base"
    printf -- "--build-arg BUILD_IMAGE_VERSION=%s " "$base_version"
    printf -- "--progress plain "

    if [[ "$dockerfile" != "Dockerfile" ]]
    then
        printf -- "--file %s " "$dockerfile"
    fi

    printf -- "%s " "$build_context"
}


function generate_command() {
    build_image_name=$1; shift;

    printf -- "docker image build "
    echo "$build_image_name" | tr '-' '\n' | parse_arguments

    for i in "$@"
    do
        printf -- "%s " "$i"
    done

    printf -- "\\n"
}

function build_custom_if_needed() {
    build_image_name=$1
    full_image_name="$CI_REGISTRY_IMAGE:$build_image_name"

    docker_command=$(generate_command "$@")

    if [[ "${FORCE_BUILD:-false}" == "true" ]]
    then
        docker_command="$docker_command --no-cache"

        echo "Force building $build_image_name due to \$FORCE_BUILD=true with $docker_command"
    else
        docker_command="$docker_command --cache-from $full_image_name"

        echo "Building $build_image_name with $docker_command"
    fi


    eval "$docker_command"
}

build_custom_if_needed "$@"
