#!/usr/bin/env sh
#
# Script to rebuild the locales inside the container to be en_US.UTF-8

if [ ! -f /etc/default/locale ]
then
    apt-get update
    apt-get install -yq --no-install-recommends locales

    sed -i 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/g' /etc/locale.gen

    locale-gen "$LC_ALL"

    # Cleanup the container
    apt-get autoremove -yq
    apt-get clean -yq
    rm -rf /var/lib/apt/lists/*
    rm -rf /tmp/*
    rm -rf /root/.cache
    rm -rf /root/*
fi
