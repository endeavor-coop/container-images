#!/usr/bin/env bash

set -xeuo pipefail
IFS=$'\n\t'

version="$1"
name="pnpm"

export PNPM_HOME="/pnpm"
export PATH="$PNPM_HOME:$PATH"


if [[ "$version" != "latest" ]]
then
    name="$name@$version"
fi

corepack enable "$name"
