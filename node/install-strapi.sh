#!/usr/bin/env bash

set -xeuo pipefail
IFS=$'\n\t'

version="$1"

# Install our strapi version globally
yarn global add "@strapi/strapi@${version}"

# Clean yarn cache
yarn cache clean --force

rm -rf /tmp/*
rm -rf /root/.cache
rm -rf /root/*
