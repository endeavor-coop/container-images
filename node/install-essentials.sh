#!/usr/bin/env bash

set -xeuo pipefail
IFS=$'\n\t'

apt-get update

apt-get install --no-install-recommends -yq \
    iputils-ping

apt-get autoremove -yq
apt-get clean -yq
rm -rf /var/lib/apt/lists/*
rm -rf /tmp/*
rm -rf /root/.cache
rm -rf /root/*
