#!/usr/bin/env bash

set -Eeuo pipefail

if [ "$1" = "strapi" ]; then

  if [ ! -f "package.json" ]; then

    DATABASE_CLIENT=${DATABASE_CLIENT:-sqlite}
    DATABASE_HOST=${DATABASE_HOST:-}
    DATABASE_PORT=${DATABASE_PORT:-}
    DATABASE_NAME=${DATABASE_NAME:-}
    DATABASE_USERNAME=${DATABASE_USERNAME:-}
    DATABASE_PASSWORD=${DATABASE_PASSWORD:-}
    DATABASE_SSL=${DATABASE_SSL:-}

    EXTRA_ARGS=${EXTRA_ARGS:-}

    echo "Using strapi $(strapi version)"
    echo "No project found at /app. Creating a new strapi project"

    DOCKER=true strapi new . \
      --dbclient="$DATABASE_CLIENT" \
      --dbhost="$DATABASE_HOST" \
      --dbport="$DATABASE_PORT" \
      --dbname="$DATABASE_NAME" \
      --dbusername="$DATABASE_USERNAME" \
      --dbpassword="$DATABASE_PASSWORD" \
      --dbssl="$DATABASE_SSL" \
      "$EXTRA_ARGS"

  elif [ ! -d "node_modules" ] || [ ! "$(ls -qAL node_modules 2>/dev/null)" ]; then

    echo "Node modules not installed. Installing..."

    if [ -f "yarn.lock" ]; then

      yarn install

    else

      npm install

    fi

  fi

  # Add graphql plugin support for strapi if it does not currently exist
  #
  # Request: https://gitlab.com/endeavor-coop/container-images/-/issues/7
  # Documentation: https://docs.strapi.io/developer-docs/latest/plugins/graphql.html
  grep "@strapi/plugin-graphql" package.json > /dev/null || yarn strapi install graphql

fi

echo "Starting your app..."

exec "$@"
