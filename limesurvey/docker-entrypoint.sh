#!/usr/bin/env bash

set -Eeuo pipefail

# Supported environment variables

# DATABASE_TYPE (optional) [pgsql] = supports mysql, pgsql
# DATABASE_HOST (required) [<empty>] = unix:// (unix file socket) or IP/hostname
# DATABASE_PORT (optional) [<empty>] = port used for connecting to database host
# DATABASE_NAME (optional) [limesurvey] = database name to use
# DATABASE_TABLE_PREFIX (optional) [limesurvey_] = table prefix to use
# DATABASE_USERNAME (optional) [limesurvey] = database username to use for connections
# DATABASE_PASSWORD (required) [<empty>] = database password
# ADMIN_NAME (optional) [Administrator] = override the default administrator's name
# ADMIN_EMAIL (optional) [<empty>] = override the default administrator's email
# ADMIN_USERNAME (optional) [admin] = override the default administrator's username
# ADMIN_PASSWORD (required) [<empty>] = set the administrator's initial password
# BASE_URL (optional) [<empty>] = base application URL
# PUBLIC_URL (optional) [<empty>] = public URL for the application to generate URL's for
# URL_FORMAT (optional) [get] = get or path

# Defaults
DATABASE_TYPE="${DATABASE_TYPE:-pgsql}"
DATABASE_HOST="${DATABASE_HOST:-}"
DATABASE_PORT="${DATABASE_PORT:-}"
DATABASE_NAME="${DATABASE_NAME:-limesurvey}"
DATABASE_TABLE_PREFIX="${DATABASE_TABLE_PREFIX:-limesurvey_}"
DATABASE_USERNAME="${DATABASE_USERNAME:-limesurvey}"
DATABASE_PASSWORD="${DATABASE_PASSWORD:-limesurvey}"
ADMIN_NAME="${ADMIN_NAME:-admin}"
ADMIN_EMAIL="${ADMIN_EMAIL:-admin@example.com}"
ADMIN_USERNAME="${ADMIN_USERNAME:-admin}"
ADMIN_PASSWORD="${ADMIN_PASSWORD:-password}"
BASE_URL="${BASE_URL:-}"
PUBLIC_URL="${PUBLIC_URL:-}"
URL_FORMAT="${URL_FORMAT:-get}"
DEBUG="${DEBUG:-0}"

# Wait for a service:port to be open
#
# Example:
#   wait_for_port "postgres" "5432"
wait_for_port() {
    local host="$1" port="$2"
    local index=0
    local TRY_LOOP=10

    while ! nc -z "$host" "$port" >/dev/null 2>&1 < /dev/null; do
        index=$((index + 1))

        if [ "$index" -ge "$TRY_LOOP" ]; then
            echo >&2 "$host:$port still not reachable, giving up"
            exit 1
        fi

        echo "waiting for $host ... $index/$TRY_LOOP"
        sleep 5

    done
}

create_application_config() {
    if [[ "$DATABASE_TABLE_PREFIX" != *\_ ]]
    then
        DATABASE_TABLE_PREFIX="${DATABASE_TABLE_PREFIX}_"
    fi

    cat <<EOF > application/config/config.php
<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

return [
    'components' => [
        'db' => [
            'connectionString' => '$DATABASE_TYPE:$DATABASE_CONNECT=$DATABASE_HOST;port=$DATABASE_PORT;dbname=$DATABASE_NAME;',
            'emulatePrepare' => true,
            'username' => '$DATABASE_USERNAME',
            'password' => '$DATABASE_PASSWORD',
            'charset' => '$DATABASE_CHARSET',
            'tablePrefix' => '$DATABASE_TABLE_PREFIX',
        ],

        'request' => [
            'baseUrl' => '$BASE_URL',
        ],

        'session' => [
            'sessionName'=>'LS-WLEFWTHKNTUIFPPE'
            // Uncomment the following lines if you need table-based sessions.
            // Note: Table-based sessions are currently not supported on MSSQL server.
            // 'class' => 'application.core.web.DbHttpSession',
            // 'connectionID' => 'db',
            // 'sessionTableName' => '{{sessions}}',
        ],

        'urlManager' => [
            'urlFormat' => '$URL_FORMAT',
            'rules' => [],
            'showScriptName' => true,
        ],

    ],

    'config' => [
        'publicurl' => '$PUBLIC_URL',
        'debug' => $DEBUG,
        'debugsql' => $DEBUG,
    ],

    // For security issue : it's better to set runtimePath out of web access
    // Directory must be readable and writable by the webuser
    // 'runtimePath'=>'/var/cache/limesurvey/'
];


EOF

}

case "$1" in
    fpm )
        echo "Starting php-fpm service ... ($0)"

        if [[ ! -f /var/www/html/index.php ]]
        then
            /usr/local/bin/install-limesurvey.sh
        fi

        if [[ -z "$DATABASE_HOST" ]]
        then
            echo "ERROR: Missing DATABASE_HOST environment variable configuration."
            exit 1
        fi

        if [[ "$DATABASE_HOST" == unix\:\/\/* ]]
        then
            DATABASE_CONNECT="unix_socket"
        else
            DATABASE_CONNECT="host"

            wait_for_port "$DATABASE_HOST" "$DATABASE_PORT"
        fi

        case "$DATABASE_TYPE" in
            mysql )
                DATABASE_CHARSET=${DATABASE_CHARSET:-'utf8mb4'}
                ;;

            * )
                DATABASE_CHARSET=${DATABASE_CHARSET:-'utf8'}
                ;;
        esac

        # Create the application configuration file if it does not exist.
        if [[ ! -f application/config/config.php ]]
        then
            echo "Creating config"
            create_application_config
        else
            echo "Not creating config"
        fi

        # Provision the application if the database is not already configured
        if ! php application/commands/console.php updatedb
        then
            php application/commands/console.php install "$ADMIN_USERNAME" "$ADMIN_PASSWORD" "$ADMIN_NAME" "$ADMIN_EMAIL"
        fi

        eval "php-fpm"

        ;;

    * )
        eval "$@"
        ;;
esac
