#!/usr/bin/env bash
#
# Download, extract, and install Limesurvey into /var/www/html/

set -xeuo pipefail

# Only download and install if the html directory is empty
if [[ ! -f /var/www/html/index.php ]]
then
    # Download limesurvey
    curl -SL "https://github.com/LimeSurvey/LimeSurvey/archive/${LIMESURVEY_VERSION}.tar.gz" --output /tmp/limesurvey.tar.gz

    # Validate that the file we downloaded matches the sha256sum
    # Validate that the archive passes validation
    if [[ $(sha256sum "/tmp/limesurvey.tar.gz") != "$LIMESURVEY_SHA256SUM  /tmp/limesurvey.tar.gz" ]]
    then
        echo "Failed file validation"
        exit 1
    fi

    # Extract and configure limesurvey
    mkdir -p /tmp/limesurvey
    tar -xzf "/tmp/limesurvey.tar.gz" --strip-components=1 -C /tmp/limesurvey

    # If we are updating the application but carrying over a previous 'upload'
    # directory, we need to copy the files to the file destination differently.
    #
    # When the upload directory does not exist or if the directory is empty,
    # perform a full copy of limesurvey to the /var/www/html/ directory,
    # else perform a partial copy, excluding the upload directory.
    if [[ -d "/var/www/html/upload" || -n "$(ls -A /var/www/html/upload)" ]]
    then
        rm -rf /tmp/limesurvey/upload
    fi

    cp -rp /tmp/limesurvey/* /var/www/html/

    # Make sure all of the upload directories exist
    mkdir -p /var/www/html/upload/admintheme
    mkdir -p /var/www/html/upload/surveys
    mkdir -p /var/www/html/upload/themes
    mkdir -p /var/www/html/upload/themes/survey
    mkdir -p /var/www/html/upload/themes/survey/generalfiles
    mkdir -p /var/www/html/upload/labels

    # Directory does not exist in the limesurvey sources, so make sure it still exists
    # as it is required for the site to function.
    mkdir -p /var/www/html/tmp/runtime/cache/

    chown -R www-data:www-data /var/www/html/

    rm -rf /tmp/*
fi

if [[ -z "$(ls -A /var/www/html/upload)" ]]
then
    mkdir -p /var/www/html/upload/admintheme
    mkdir -p /var/www/html/upload/surveys
    mkdir -p /var/www/html/upload/themes
    mkdir -p /var/www/html/upload/themes/survey
    mkdir -p /var/www/html/upload/themes/survey/generalfiles
    mkdir -p /var/www/html/upload/labels

    chown -R www-data:www-data /var/www/html/
fi
