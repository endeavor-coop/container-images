# syntax=docker/dockerfile:experimental
#
# The arguments in this file are configured in
# /scripts/build-image.sh
#

ARG BUILD_IMAGE_NAME
ARG BUILD_IMAGE_VERSION
FROM ${BUILD_IMAGE_NAME}:${BUILD_IMAGE_VERSION}

ENV LANG=en_US.UTF-8 \
    LANGUAGE=en_US:en \
    LC_ALL=en_US.UTF-8 \
    PYTHONPATH=/app \
    REQUESTS_CA_BUNDLE=/etc/ssl/certs/ca-certificates.crt \
    TMPDIR=/tmp

# Install / rebuild locales to make sure we have the correct language
# set within the container for C string operations.
COPY scripts/rebuild-locales.sh /usr/local/bin/
RUN /usr/local/bin/rebuild-locales.sh

WORKDIR /app

COPY /python/ /usr/local/bin/build-python/

# Install all of our essentials for building a workable python container image.
RUN /usr/local/bin/build-python/install-essentials.sh

# Add library support for progress database connections using odbc.
COPY /python/lib/ddoe27.so /python/lib/libddicu27.so /usr/lib/

# Install the base pip packages needed for all python images.
ARG BASE_VERSION=0
RUN /usr/local/bin/build-python/install-base.sh ${BASE_VERSION}

# Install the fastapi package for being able to build an http API service.
ARG FASTAPI_VERSION
RUN if [ -n "$FASTAPI_VERSION" ]; then /usr/local/bin/build-python/install-fastapi.sh ${FASTAPI_VERSION}; fi

# Install the thirdparty package for being able to add third party utilities.
ARG THIRDPARTY_VERSION
RUN if [ -n "$THIRDPARTY_VERSION" ]; then /usr/local/bin/build-python/install-thirdparty.sh ${THIRDPARTY_VERSION}; fi

# Install the dbt package for bineg able to run dbt workloads.
ARG DBT_VERSION
RUN if [ -n "$DBT_VERSION" ]; then /usr/local/bin/build-python/install-dbt.sh ${DBT_VERSION}; fi

# If we have a  install all extra pip packages
RUN if [ -f "/usr/local/bin/build-python/requirements.txt" ]; then /usr/local/bin/build-python/install-generic-requirements.sh; fi

# Make sure we regenerate the localisation files for the libc library to properly
# support the en_US UTF-8 character encoding after we have installed of our packages.
RUN locale-gen ${LANG}
