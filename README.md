# Container Build Images

This repository is responsible for building container images that help speed up all of the container building and testing for all projects across the endeavor-coop group.

It is heavily inspired by the [gitlab-org/gitlab-build-images](https://gitlab.com/gitlab-org/gitlab-build-images) repository.

## How to build

1. Open a bash shell capable terminal on your machine (Git Bash for Windows).
2. Change to this repositories location on your file system.
3. Execute `./scripts/build-local.sh`

## Supported CI_JOB_NAME build targets

1. pipeline
1. python-3.9
1. python-3.9-dbt-0.19
1. python-3.9-fastapi-0.65
1. node-16.13-strapi-4.0.0
1. node-16.14-strapi-4.0.0
1. node-16.13-strapi-4.1.3
1. node-16.14-strapi-4.1.3
1. node-16.15-strapi-4.2.0
1. node-16.16-strapi-4.3.2
1. node-16.18-strapi-4.4.3
1. node-16.18-strapi-4.12.1
1. node-20.10-strapi-4.16.2
