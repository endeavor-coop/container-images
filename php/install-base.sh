#!/usr/bin/env bash

set -xeuo pipefail
IFS=$'\n\t'

# Install all of our system packages
apt-get update

apt-get install -yq --no-install-recommends \
    libldap2-dev \
    libfreetype6-dev \
    libjpeg-dev \
    libonig-dev \
    zlib1g-dev \
    libc-client-dev \
    libkrb5-dev \
    libpng-dev \
    libpq-dev \
    libzip-dev \
    netcat

# Make sure that the libldap.so library is loaded into ldconfig
ldconfig

# Configure php extensions for installation
docker-php-ext-configure gd
docker-php-ext-configure imap --with-kerberos --with-imap-ssl

# Install php extensions
docker-php-ext-install -j5 \
    exif \
    gd \
    imap \
    ldap \
    mbstring \
    pdo \
    pdo_pgsql \
    pgsql \
    zip \
    pdo_mysql

# Cleanup the container
apt-get autoremove -yq
apt-get clean -yq
rm -rf /var/lib/apt/lists/*
rm -rf /tmp/*
rm -rf /root/.cache
rm -rf /root/*
