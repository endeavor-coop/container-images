#!/usr/bin/env bash

set -xeuo pipefail
IFS=$'\n\t'

version="$1"

apt-get update
apt-get install --no-install-recommends -yq \
    build-essential

TMPDIR=/tmp pip install --cache-dir /tmp --compile --requirement "/usr/local/bin/build-python/requirements.fastapi.$version.txt"

apt-get remove build-essential -y
apt-get autoremove -yq
apt-get clean -yq
rm -rf /var/lib/apt/lists/*
rm -rf /tmp/*
rm -rf /root/.cache
rm -rf /root/*
