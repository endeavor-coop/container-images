#!/usr/bin/env bash

set -xeuo pipefail
IFS=$'\n\t'

base_version="$1"

apt-get update
apt-get install --no-install-recommends -yq \
    wget \
    gnupg2 \
    patch

echo "deb http://apt.postgresql.org/pub/repos/apt/ bookworm-pgdg main" > /etc/apt/sources.list.d/pgdg.list
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add -

# Setup Microsoft gpg key and apt list for installing msodbcsql support
curl -fsSL https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor -o /usr/share/keyrings/microsoft-prod.gpg
curl https://packages.microsoft.com/config/debian/12/prod.list | tee /etc/apt/sources.list.d/mssql-release.list

apt-get update

ACCEPT_EULA=Y apt-get install --no-install-recommends -yq \
    build-essential \
    default-libmysqlclient-dev \
    libldap2-dev \
    libpq-dev \
    libsasl2-dev \
    unixodbc-dev \
    pkg-config \
    msodbcsql17 \
    msodbcsql18

# Makes it so that progress database support is available from the libraries in /etc/lib/.
ldconfig

# Make sure we have the most up-to-date verisons of pip inside the container before we start installing packages
pip install --upgrade pip setuptools

TMPDIR=/tmp pip install --cache-dir /tmp --compile --requirement "/usr/local/bin/build-python/requirements.base.$base_version.txt"

# Get the site-packages directory from python itself.
# site_packages=$(python -c "import site; print(site.getsitepackages()[0])")

apt-get remove -yq \
    build-essential \
    gnupg2 \
    patch \
    wget

apt-get autoremove -yq
apt-get clean -yq
rm -rf /var/lib/apt/lists/*
rm -rf /tmp/*
rm -rf /root/.cache
rm -rf /root/*
