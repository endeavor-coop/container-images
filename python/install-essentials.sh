#!/usr/bin/env bash

set -xeuo pipefail
IFS=$'\n\t'

export DEBIAN_FRONTEND=noninteractive

apt-get update
apt-get install --no-install-recommends -yq \
    curl \
    ca-certificates \
    locales \
    ncat

# Set UTF-8
# http://stackoverflow.com/a/3182519/2137281
LOC=$'LC_ALL=en_US.UTF-8\nLANG=en_US.UTF-8'
echo "$LOC" > /etc/environment
echo "en_US.UTF-8 UTF-8" > /etc/locale.gen
locale-gen
dpkg-reconfigure locales -f noninteractive -p critical
locale -a

# Set the correct local timezone to Eastern
if [[ -f "/etc/localtime" ]]
then
    unlink /etc/localtime
fi
ln -s /usr/share/zoneinfo/America/New_York /etc/localtime

apt-get autoremove -yq
apt-get clean -yq
rm -rf /var/lib/apt/lists/*
