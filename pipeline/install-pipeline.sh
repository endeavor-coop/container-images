#!/usr/bin/env ash
# shellcheck shell=dash

set -euxo pipefail

apk add --no-cache \
    bash \
    build-base \
    curl \
    gettext \
    git \
    jq \
    libffi-dev \
    openssl \
    openssl-dev \
    py3-pip \
    python3-dev \
    rsync \
    shellcheck

# We are building into a container, we are not managing like a normal system operates, so let's just bypass the externally managed
rm -f /usr/lib/python3.*/EXTERNALLY-MANAGED

# Install rust
curl -sSf https://sh.rustup.rs | sh -s -- -y

export PATH=$HOME/.cargo/bin:$PATH

# Make sure to install pip > 21.X to use the new resolver for constraint locking.
pip3 install --upgrade pip setuptools

# Install pip packages using the legacy-resolver for dependency calculation.
TMPDIR=/tmp pip3 install --cache-dir /tmp --compile --requirement "/usr/local/bin/build-pipeline/requirements.txt"

apk del \
    build-base \
    libffi-dev \
    openssl-dev \
    python3-dev

# Uninstall rust
rustup self uninstall -y

rm -rf /tmp/*
rm -rf /root/.cache
rm -rf /root/*
