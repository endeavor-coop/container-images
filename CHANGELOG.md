# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project adheres to [Calendar Versioning](https://calver.org/).

## [22.03.28] - 2022-03-28

### Added:

-   Support for building strapi 4.1.3 on node 16.13
-   Added the latest LTS release builds for node on 16.14.

### Fixed:

-   "fixed" dbt installation to just installing the dbt-postgres integration on version 1.0.4, instead of just whatever is latest. As of version 1.0.0 of dbt, the `pip install dbt` method is no longer supported and you will need to install a specific integration.

## [21.06.10] - 2021-06-10

### Added:

-   Added python-jose and python-ldap to the base python bundle building process.

## [21.05.25] - 2021-05-25

### Added:

-   Updated all of the library package versions within the repository to their latest versions for security and feature updates.
-   Added constraint support to the pipeline image for future usage in all locations using pip for package management.

### Fixed:

-   Do not pin dbt to 0.19.1 until constraint support is added to the python images.

## [21.03.23] - 2021-03-23

### Added:

-   Added 'downstream' limesurvey image build for version 3.25.18+210316 and 4.4.13+210315.

## [21.03.04] - 2021-03-04

### Added:

-   Establish a base php-fpm image for versions 7.4 and 8.0 to host any PHP based application we might need.

### Fixed:

-   Fix the locales in all debian based images to correctly use the en_US.UTF-8 locale.

## [21.03.02] - 2021-03-02

### Added:

-   Updated all pip requirements packages to their latest versions in both python and pipeline container images.

### Fixed:

-   Corrected the fastapi version reference to the actual version being installed, 0.63 not 0.61.
-   Downgrade dbt to 0.18.1 until 0.19.1 is released for missing fixed packages in 0.19.0 release

## [21.02.25] - 2021-02-25

### Added:

-   Install rust for building the python cryptography library from source during the base package installation. This was a 'breaking' change in [cryptography 3.4](https://github.com/pyca/cryptography/blob/main/CHANGELOG.rst#34---2021-02-07).

## [20.11.30] - 2020-11-30

### Added:

-   Added MIT LICENSE to project and opened to public on gitlab.com.
-   Updated README.md to use python 3.9 instead of 3.8 everywhere.

## [20.11.04] - 2020-11-04

### Added

-   Added renovate support to the repository to maintain package versions automatically.
-   Refactored pipeline image build process to more align with the python build style.
-   Added CODEOWNERS to repository.

## [20.11.03] - 2020-11-03

### Added

-   Add support for the build-and-push container-image job to be run against a global CI/CD schedule.
-   Add python-3.8 and python-3.8-fastapi-0.16 to container-image:build-and-push process for maintaining patch levels on all software packages.
-   Create python-3.8 image with dbt-0.18 support as python-3.8-dbt-0.18 and add pipeline build support for it.
-   Create scheduled process for supporting continuous building of images based upon a daily schedule or each individual day of the week as a weekly schedule.

## [20.10.30] - 2020-10-30

### Added

-   Created python images for running python-3.8 and python-3.8-fastapi-0.61
-   Created pipeline image for running all of our pipeline tasks without having to waste time installing packages during the execution of a task.
-   Established CHANGELOG.md for maintaining a human readable change log.
